## Day1
The application creates api list, create, update, delete api  using generic views according to models. Api urls are http://127.0.0.1:8000/list-create-api/ and http://127.0.0.1:8000/product/1/ . 1 must be a pk of Product model.

### Insallation
To run this application, you can follow below instructions.

Firstly create a .env file for environment variables .env file can be like this:

SECRET_KEY=m7zu4r6=s=6t*7_^m!=%w^85yfw#ec7v*y79$*vs)7fm%g65kl

DEBUG=True

ALLOWED_HOSTS=127.0.0.1

```bash
sudo apt-get install gettext
git clone https://gitlab.com/erdogan-codeck/day1.git
cd day1
python -m venv your_venv
source your_venv/bin/activate
pip install -r requirements.py
python manage.py makemigrations
python manage.py
python manage.py loaddata category
python manage.py loaddata products
python manage.py makemessages -l 'tr' --ignore=your_venv
python manage.py compilemessages
```
optional: If you want to see changes according to django.po file on http://127.0.0.1:8000/ , run below command please.
```bash
python manage.py compilemessages
```

If 


### Result

![Alt Text](day1.gif)
