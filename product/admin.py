from django.contrib import admin

from .models import Product, Category

class ProductInline(admin.TabularInline):
    model = Product


class CategoryAdmin(admin.ModelAdmin):
    inlines = [
        ProductInline,
    ]

admin.site.register(Category, CategoryAdmin)