from django.db import models
from django.utils.text import slugify

# Create your models here.
class Category(models.Model):
    name = models.CharField("kategori adı", max_length=150)

    def __str__(self):
        return self.name


class Product(models.Model):    
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name = "kategori")
    name = models.CharField("ürün adı", max_length=100, db_index=True)
    description = models.TextField("Ürün açıklaması", blank=True)
    price = models.DecimalField("gerçek fiyat", max_digits=10, decimal_places=2, default = 0)
    weight = models.FloatField("Ağırlık", default = 0)

    def __str__(self):
        return self.name
	