from .models import Product, Category
from rest_framework import serializers


class EnProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['pk', 'name_en', 'description_en', 'category']

class TrProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['pk', 'name_tr', 'description_tr', 'category']


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['pk,''name', 'slug']


# class ProductSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Product
#         fields = ['pk','name', 'category', 'language', 'slug', 'description', 'price', "weight"]
        