from modeltranslation.translator import register, TranslationOptions
from .models import Product

@register(Product)
class UserTranslationOptions(TranslationOptions):
    fields = ('name', 'description', )