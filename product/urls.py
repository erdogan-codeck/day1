from django.urls import path

from . import views


urlpatterns = [
    path('', views.index),
    path('list-create-api/', views.ProductListCreate.as_view()),
    path('product/<int:pk>/', views.ProductGetUpdateDelete.as_view())
]
