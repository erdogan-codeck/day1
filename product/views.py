from django.shortcuts import render
from django.utils.translation import gettext as _
from django.utils.translation import get_language


from rest_framework import generics


from .models import Product, Category
from .serializers import EnProductSerializer, TrProductSerializer, CategorySerializer


class ProductListCreate(generics.ListCreateAPIView):
    queryset = Product.objects.all()
    
    def get_serializer_class(self):
        if 'tr' == get_language():
            return TrProductSerializer
        return EnProductSerializer

class ProductGetUpdateDelete(generics.RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all()
    
    def get_serializer_class(self):
        if 'tr' == get_language():
            return TrProductSerializer
        return EnProductSerializer
    

def index(request):
    text = _("this is some random text")
    return render(request, 'home.html', { 'text': text })
